package shop.besthsg.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import shop.besthsg.api.member.entity.Member;
import shop.besthsg.api.member.model.MemberCreateRequest;
import shop.besthsg.api.member.model.PasswordChangeRequest;
import shop.besthsg.api.member.repository.MemberRepository;
import shop.besthsg.common.enums.MemberGroup;
import shop.besthsg.common.exception.CMissingDataException;
import shop.besthsg.common.exception.CWrongPhoneNumberException;
import shop.besthsg.common.function.CommonCheck;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    public void setFirstMember() {
        String username = "superadmin";
        String password = "jfjein5awa";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberCreateRequest createRequest = new MemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setName("최고관리자");

            setMember(MemberGroup.ROLE_ADMIN, createRequest);
        }

    }

    public void setMember(MemberGroup memberGroup, MemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CWrongPhoneNumberException(); // 유효한 아이디 형식이 아닙니다로 던지기
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe())) throw new CWrongPhoneNumberException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CWrongPhoneNumberException(); // 중복된 아이디가 존재합니다 던지기

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));

        Member member = new Member.MemberBuilder(memberGroup, createRequest).build();
        memberRepository.save(member);

    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }

    public void putPassword(long memberId, PasswordChangeRequest changeRequest, boolean isAdmin) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);

        if (!isAdmin && !passwordEncoder.matches(changeRequest.getChangePassword(), member.getPassword())) throw new CMissingDataException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!changeRequest.getChangePassword().equals(changeRequest.getChangePasswordRe())) throw new CWrongPhoneNumberException(); // 변경할 비밀번호가 일치하지 않습니다. 던지기

        String passwordResult = passwordEncoder.encode(changeRequest.getChangePassword());
        member.putPassword(passwordResult);
        memberRepository.save(member);
    }
}
