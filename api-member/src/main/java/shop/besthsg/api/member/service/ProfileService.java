package shop.besthsg.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import shop.besthsg.api.member.entity.Member;
import shop.besthsg.api.member.model.MemberProfileResponse;
import shop.besthsg.api.member.repository.MemberRepository;
import shop.besthsg.common.exception.CAccessDeniedException;
import shop.besthsg.common.exception.CMissingDataException;

@Service
@RequiredArgsConstructor
public class ProfileService {
    private final MemberRepository memberRepository;

    public Member getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Member member = memberRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!member.getIsEnabled()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return member;
    }

    public MemberProfileResponse getProfile() {
        Member member = getMemberData();
        return new MemberProfileResponse.Builder(member).build();
    }
}
