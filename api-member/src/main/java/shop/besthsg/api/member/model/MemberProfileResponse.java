package shop.besthsg.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.api.member.entity.Member;
import shop.besthsg.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberProfileResponse {
    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원 아이디")
    private String username;

    @ApiModelProperty(notes = "회원 이름")
    private String name;

    @ApiModelProperty(notes = "회원 활성화 여부")
    private Boolean isEnabled;

    private MemberProfileResponse(Builder builder) {
        this.memberId = builder.memberId;
        this.username = builder.username;
        this.name = builder.name;
        this.isEnabled = builder.isEnabled;

    }

    public static class Builder implements CommonModelBuilder<MemberProfileResponse> {
        private final Long memberId;
        private final String username;
        private final String name;
        private final Boolean isEnabled;

        public Builder(Member member) {
            this.memberId = member.getId();
            this.username = member.getUsername();
            this.name = member.getName();
            this.isEnabled = member.getIsEnabled();
        }
        @Override
        public MemberProfileResponse build() {
            return new MemberProfileResponse(this);
        }
    }
}
