package shop.besthsg.common.exception;

public class CUsernameSignInFailedException extends RuntimeException {
    public CUsernameSignInFailedException(String msg, Throwable t) {
        super(msg, t);
    }

    public CUsernameSignInFailedException(String msg) {
        super(msg);
    }

    public CUsernameSignInFailedException() {
        super();
    }
}

