package shop.besthsg.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberGroup {
    ROLE_ADMIN("최고관리자")
    , ROLE_TEAM_LEADER("팀장")
    , ROLE_USER("사원");

    private final String name;

}
