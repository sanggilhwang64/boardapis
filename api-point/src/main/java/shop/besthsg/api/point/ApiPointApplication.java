package shop.besthsg.api.point;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPointApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiPointApplication.class, args);
    }

}
