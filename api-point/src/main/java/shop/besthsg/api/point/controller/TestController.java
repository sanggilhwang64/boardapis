package shop.besthsg.api.point.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.besthsg.api.point.model.MemberProfileResponse;
import shop.besthsg.api.point.model.MyPointResponse;
import shop.besthsg.api.point.service.MemberInfoService;
import shop.besthsg.api.point.service.PointService;
import shop.besthsg.common.response.model.CommonResult;
import shop.besthsg.common.response.model.SingleResult;
import shop.besthsg.common.response.service.ResponseService;

@Api(tags = "권한 테스트")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/test")
public class TestController {
    private final MemberInfoService memberInfoService;
    private final PointService pointService;

    @ApiOperation(value = "내 포인트 정보")
    @GetMapping("/info")
    public SingleResult<MyPointResponse> getProfile() {
        MemberProfileResponse memberProfileResponse = memberInfoService.getProfile();
        return ResponseService.getSingleResult(pointService.getMyPoint(memberProfileResponse));
    }

    @ApiOperation(value = "관리자만 접근 가능")
    @GetMapping("/admin")
    public CommonResult testAdmin() {
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "유저만 접근 가능")
    @GetMapping("/user")
    public CommonResult testUser() {
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "로그인 사용자 전체 접근 가능")
    @GetMapping("/all")
    public CommonResult testAll() {
        return ResponseService.getSuccessResult();
    }

}
