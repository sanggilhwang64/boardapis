package shop.besthsg.api.point.lib;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

public class RestApi {
    public <T1, T2> T2 getApiPostJsonResponse(String apiUrl, T1 requestData, Class<T2> responseModel) throws Exception {
        return this.getApiJsonResponse(HttpMethod.POST, apiUrl, requestData, responseModel);
    }

    public <T> T getApiGetJsonResponse(String apiUrl, Class<T> responseModel) throws Exception {
        return this.getApiJsonResponse(HttpMethod.GET, apiUrl, null, responseModel);
    }

    private <T1, T2> T2 getApiJsonResponse(HttpMethod httpMethod, String apiUrl, T1 requestData, Class<T2> responseModel) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String token = authentication.getName();

        RestTemplate restTemplate = new RestTemplateBuilder().build();

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiUrl);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set("Authorization", "Bearer " + token);

        HttpEntity<T1> httpEntity = new HttpEntity<>(requestData, httpHeaders);
        if (httpMethod.equals(HttpMethod.GET)) httpEntity = new HttpEntity<>(null, httpHeaders);

        return restTemplate.exchange(builder.toUriString(), httpMethod, httpEntity, responseModel).getBody();
    }

    public <T1, T2> T2 getApiPostJsonResponseNonToken(String apiUrl, T1 requestData, Class<T2> responseModel) throws Exception {
        return this.getApiJsonResponseNonToken(HttpMethod.POST, apiUrl, requestData, responseModel);
    }

    public <T> T getApiGetJsonResponseNonToken(String apiUrl, Class<T> responseModel) throws Exception {
        return this.getApiJsonResponseNonToken(HttpMethod.GET, apiUrl, null, responseModel);
    }

    private <T1, T2> T2 getApiJsonResponseNonToken(HttpMethod httpMethod, String apiUrl, T1 requestData, Class<T2> responseModel) throws Exception {
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiUrl);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<T1> httpEntity = new HttpEntity<>(requestData, httpHeaders);
        if (httpMethod.equals(HttpMethod.GET)) httpEntity = new HttpEntity<>(null, httpHeaders);

        return restTemplate.exchange(builder.toUriString(), httpMethod, httpEntity, responseModel).getBody();
    }

}
