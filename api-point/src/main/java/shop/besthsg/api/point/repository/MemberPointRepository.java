package shop.besthsg.api.point.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.besthsg.api.point.entity.MemberPoint;

public interface MemberPointRepository extends JpaRepository<MemberPoint, Long> {
}
