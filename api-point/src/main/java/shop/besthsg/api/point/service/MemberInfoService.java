package shop.besthsg.api.point.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import shop.besthsg.api.point.lib.RestApi;
import shop.besthsg.api.point.model.MemberProfileResponse;
import shop.besthsg.common.exception.CMissingDataException;

@Service
public class MemberInfoService {
    @Value("${api.host.resource.member}")
    private String API_HOST_RESOURCE_MEMBER;

    public MemberProfileResponse getProfile() {
        String apiUrl = this.API_HOST_RESOURCE_MEMBER + "/v1/member/profile";
        try {
            return new RestApi().getApiGetJsonResponse(apiUrl, MemberProfileResponse.class);
        } catch (Exception e) {
            throw new CMissingDataException();
        }
    }
}
