package shop.besthsg.api.point.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyPointResponse {
    @ApiModelProperty(notes = "회원시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원이름")
    private String memberName;

    @ApiModelProperty(notes = "포인트")
    private Long pointValue;

    private MyPointResponse(Builder builder) {
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.pointValue = builder.pointValue;
    }

    public static class Builder implements CommonModelBuilder<MyPointResponse> {
        private final Long memberId;
        private final String memberName;
        private final Long pointValue;

        public Builder(Long memberId, String memberName, Long pointValue) {
            this.memberId = memberId;
            this.memberName = memberName;
            this.pointValue = pointValue;

        }

        @Override
        public MyPointResponse build() {
            return new MyPointResponse(this);
        }
    }
}
