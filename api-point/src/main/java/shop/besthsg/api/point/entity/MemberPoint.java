package shop.besthsg.api.point.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.common.interfaces.CommonModelBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberPoint {
    @Id
    private Long memberId;

    @Column(nullable = false)
    private Long pointValue;

    private MemberPoint(Builder builder) {
        this.memberId = builder.memberId;
        this.pointValue = builder.pointValue;
    }

    public static class Builder implements CommonModelBuilder<MemberPoint> {
        private final Long memberId;
        private final Long pointValue;

        public Builder(Long memberId) {
            this.memberId = memberId;
            this.pointValue = 0L;
        }

        @Override
        public MemberPoint build() {
            return new MemberPoint(this);
        }
    }
}
