package shop.besthsg.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardDocumentUpdateRequest {
    @ApiModelProperty(notes = "게시글 제목")
    private String title;

    @ApiModelProperty(notes = "게시글 내용")
    private String contents;
}
