package shop.besthsg.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardCommentUpdateRequest {
    @ApiModelProperty(notes = "댓글 내용")
    private String commentContents;
}
