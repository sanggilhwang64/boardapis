package shop.besthsg.api.board.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import shop.besthsg.api.board.entity.BoardComment;

public interface BoardCommentRepository extends JpaRepository<BoardComment, Long> {
    Page<BoardComment> findAllByBoardIdOrderByIdDesc(long boardId, Pageable pageable);
}
