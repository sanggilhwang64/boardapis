package shop.besthsg.api.board.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.besthsg.api.board.model.BoardCommentCreateRequest;
import shop.besthsg.api.board.model.BoardCommentItem;
import shop.besthsg.api.board.model.BoardCommentUpdateRequest;
import shop.besthsg.api.board.service.BoardCommentService;
import shop.besthsg.common.response.model.CommonResult;
import shop.besthsg.common.response.model.ListResult;
import shop.besthsg.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "댓글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/comment")
public class BoardCommentController {
    private final BoardCommentService boardCommentService;

    @ApiOperation(value = "댓글 등록")
    @PostMapping("/board-id/{boardId}")
    public CommonResult setComment(@PathVariable long boardId, @RequestBody @Valid BoardCommentCreateRequest createRequest) {
        boardCommentService.setComment(boardId, createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "댓글 목록")
    @GetMapping("/all/board-id/{boardId}/page/{pageNum}")
    public ListResult<BoardCommentItem> getComments(@PathVariable long boardId, @PathVariable int pageNum) {
        return ResponseService.getListResult(boardCommentService.getComments(boardId, pageNum), true);
    }

    @ApiOperation(value = "댓글 수정")
    @PutMapping("/change/{id}")
    public CommonResult putComment(@PathVariable long id, @RequestBody @Valid BoardCommentUpdateRequest updateRequest) {
        boardCommentService.putComment(id, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "댓글 삭제")
    @DeleteMapping("/{id}")
    public CommonResult delComment(@PathVariable long id) {
        boardCommentService.delComment(id);

        return ResponseService.getSuccessResult();
    }
}
