package shop.besthsg.api.board.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import shop.besthsg.api.board.entity.BoardDocument;
import shop.besthsg.api.board.model.BoardDocumentCreateRequest;
import shop.besthsg.api.board.model.BoardDocumentItem;
import shop.besthsg.api.board.model.BoardDocumentUpdateRequest;
import shop.besthsg.api.board.repository.BoardDocumentRepository;
import shop.besthsg.common.exception.CMissingDataException;
import shop.besthsg.common.response.model.ListResult;
import shop.besthsg.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class BoardDocumentService {
    private final BoardDocumentRepository boardDocumentRepository;

    public void setDocument(BoardDocumentCreateRequest request) {
        BoardDocument addData = new BoardDocument.Builder(request).build();

        boardDocumentRepository.save(addData);
    }

    public ListResult<BoardDocumentItem> getDocuments(int pageNum) {
        Page<BoardDocument> originList = boardDocumentRepository.findAll(ListConvertService.getPageable(pageNum));

        List<BoardDocumentItem> result = new LinkedList<>();
        for (BoardDocument item : originList.getContent()) {
            result.add(new BoardDocumentItem.Builder(item).build());
        }

        return ListConvertService.settingResult(
                result,
                originList.getTotalElements(),
                originList.getTotalPages(),
                originList.getPageable().getPageNumber()
        );
    }

    public void putViewCount(long id) {
        BoardDocument originData = boardDocumentRepository.findById(id).orElseThrow(CMissingDataException::new);
        originData.putViewCount();

        boardDocumentRepository.save(originData);
    }

    public void putDocument(long id, BoardDocumentUpdateRequest updateRequest) {
        BoardDocument originData = boardDocumentRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putDocument(updateRequest);

        boardDocumentRepository.save(originData);
    }

    public void delDocument(long id) {
        boardDocumentRepository.deleteById(id);
    }

}
