package shop.besthsg.api.board.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.api.board.model.BoardCommentCreateRequest;
import shop.besthsg.api.board.model.BoardCommentUpdateRequest;
import shop.besthsg.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardComment {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "게시글 시퀀스")
    @Column(nullable = false)
    private Long boardId;

    @ApiModelProperty(notes = "댓글 내용")
    @Column(nullable = false, columnDefinition = "TEXT")
    private String commentContents;

    @ApiModelProperty(notes = "작성자명")
    @Column(nullable = false, length = 20)
    private String writerName;

    @ApiModelProperty(notes = "작성일")
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    public void putComment(BoardCommentUpdateRequest updateRequest) {
        this.commentContents = updateRequest.getCommentContents();
    }

    private BoardComment(Builder builder) {
        this.boardId = builder.boardId;
        this.writerName = builder.writerName;
        this.commentContents = builder.commentContents;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<BoardComment> {
        private final Long boardId;
        private final String commentContents;
        private final String writerName;
        private final LocalDateTime dateCreate;

        public Builder(long boardId, BoardCommentCreateRequest createRequest) {
            this.boardId = boardId;
            this.commentContents = createRequest.getCommentContents();
            this.writerName = createRequest.getWriterName();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public BoardComment build() {
            return new BoardComment(this);
        }
    }
}
