package shop.besthsg.api.board.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import shop.besthsg.api.board.entity.BoardComment;
import shop.besthsg.api.board.model.BoardCommentCreateRequest;
import shop.besthsg.api.board.model.BoardCommentItem;
import shop.besthsg.api.board.model.BoardCommentUpdateRequest;
import shop.besthsg.api.board.repository.BoardCommentRepository;
import shop.besthsg.common.exception.CMissingDataException;
import shop.besthsg.common.response.model.ListResult;
import shop.besthsg.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardCommentService {
    private final BoardCommentRepository boardCommentRepository;

    public void setComment(long boardId, BoardCommentCreateRequest createRequest) {
        BoardComment addData = new BoardComment.Builder(boardId, createRequest).build();

        boardCommentRepository.save(addData);
    }

    public ListResult<BoardCommentItem> getComments(long boardId, int pageNum) {
        Page<BoardComment> originList = boardCommentRepository.findAllByBoardIdOrderByIdDesc(
                boardId,
                ListConvertService.getPageable(pageNum, 20));

        List<BoardCommentItem> result = new LinkedList<>();

        for (BoardComment item : originList.getContent()) {
            result.add(new BoardCommentItem.Builder(item).build());
        }

        return ListConvertService.settingResult(
                result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber()
        );
    }

    public void putComment(long id, BoardCommentUpdateRequest updateRequest) {
        BoardComment originData = boardCommentRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putComment(updateRequest);

        boardCommentRepository.save(originData);
    }

    public void delComment(long id) {
        boardCommentRepository.deleteById(id);
    }
}
