package shop.besthsg.api.board.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.besthsg.api.board.model.BoardDocumentCreateRequest;
import shop.besthsg.api.board.model.BoardDocumentItem;
import shop.besthsg.api.board.model.BoardDocumentUpdateRequest;
import shop.besthsg.api.board.service.BoardDocumentService;
import shop.besthsg.common.response.model.CommonResult;
import shop.besthsg.common.response.model.ListResult;
import shop.besthsg.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "게시글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/document")
public class BoardDocumentController {
    private final BoardDocumentService boardDocumentService;

    @ApiOperation(value = "게시글 등록")
    @PostMapping("/new")
    public CommonResult setDocument(@RequestBody @Valid BoardDocumentCreateRequest request) {
        boardDocumentService.setDocument(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시글 목록")
    @GetMapping("/all/page/{pageNum}")
    public ListResult<BoardDocumentItem> getDocuments(@PathVariable int pageNum) {
        return ResponseService.getListResult(boardDocumentService.getDocuments(pageNum), true);
    }

    @ApiOperation(value = "게시글 수정")
    @PutMapping("/change/{id}")
    public CommonResult putDocument(@PathVariable long id, @RequestBody @Valid BoardDocumentUpdateRequest updateRequest) {
        boardDocumentService.putDocument(id, updateRequest);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "조회수 증가")
    @PutMapping("/view/plus/{id}")
    public CommonResult putViewCount(@PathVariable long id) {
        boardDocumentService.putViewCount(id);

        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "게시글 삭제")
    @DeleteMapping("/{id}")
    public CommonResult delDocument(@PathVariable long id) {
        boardDocumentService.delDocument(id);

        return ResponseService.getSuccessResult();
    }
}
