package shop.besthsg.api.board.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.api.board.model.BoardDocumentCreateRequest;
import shop.besthsg.api.board.model.BoardDocumentUpdateRequest;
import shop.besthsg.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardDocument {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "게시글 제목")
    @Column(nullable = false, length = 100)
    private String title;

    @ApiModelProperty(notes = "게시글 내용")
    @Column(nullable = false, columnDefinition = "TEXT")
    private String contents;

    @ApiModelProperty(notes = "작성자명")
    @Column(nullable = false, length = 20)
    private String writerName;

    @ApiModelProperty(notes = "작성일")
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "조회수")
    @Column(nullable = false)
    private Integer viewCount;

    public void putViewCount() {
        this.viewCount += 1;
    }

    public void putDocument(BoardDocumentUpdateRequest updateRequest) {
        this.title = updateRequest.getTitle();
        this.contents = updateRequest.getContents();
    }

    private BoardDocument(Builder builder) {
        this.title = builder.title;
        this.contents = builder.contents;
        this.writerName = builder.writerName;
        this.dateCreate = builder.dateCreate;
        this.viewCount = builder.viewCount;
    }

    public static class Builder implements CommonModelBuilder<BoardDocument> {
        private final String title;
        private final String contents;
        private final String writerName;
        private final LocalDateTime dateCreate;
        private final Integer viewCount;

        public Builder(BoardDocumentCreateRequest request) {
            this.title = request.getTitle();
            this.contents = request.getContents();
            this.writerName = request.getWriterName();
            this.dateCreate = LocalDateTime.now();
            this.viewCount = 0;
        }

        @Override
        public BoardDocument build() {
            return new BoardDocument(this);
        }
    }
}
