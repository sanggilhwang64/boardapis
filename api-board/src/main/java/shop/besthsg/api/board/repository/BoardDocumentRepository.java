package shop.besthsg.api.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.besthsg.api.board.entity.BoardDocument;

public interface BoardDocumentRepository extends JpaRepository<BoardDocument, Long> {
}
