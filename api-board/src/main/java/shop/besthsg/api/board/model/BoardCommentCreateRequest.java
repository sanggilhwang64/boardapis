package shop.besthsg.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BoardCommentCreateRequest {
    @ApiModelProperty(notes = "댓글 내용", required = true)
    @NotNull
    @Length(min = 2)
    private String commentContents;

    @ApiModelProperty(notes = "작성자명",required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String writerName;
}
