package shop.besthsg.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.api.board.entity.BoardComment;
import shop.besthsg.common.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardCommentItem {
    @ApiModelProperty(notes = "댓글 시퀀스")
    private Long commentId;

    @ApiModelProperty(notes = "작성자명")
    private String writerName;

    @ApiModelProperty(notes = "댓글 내용")
    private String commentContents;

    @ApiModelProperty(notes = "작성일")
    private LocalDateTime dateCreate;

    private BoardCommentItem(Builder builder) {
        this.commentId = builder.commentId;
        this.writerName = builder.writerName;
        this.commentContents = builder.commentContents;
        this.dateCreate = builder.dateCreate;

    }

    public static class Builder implements CommonModelBuilder<BoardCommentItem> {
        private final Long commentId;
        private final String writerName;
        private final String commentContents;
        private final LocalDateTime dateCreate;

        public Builder(BoardComment boardComment) {
            this.commentId = boardComment.getId();
            this.writerName = boardComment.getWriterName();
            this.commentContents = boardComment.getCommentContents();
            this.dateCreate = boardComment.getDateCreate();
        }

        @Override
        public BoardCommentItem build() {
            return new BoardCommentItem(this);
        }
    }
}
