package shop.besthsg.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.besthsg.api.board.entity.BoardDocument;
import shop.besthsg.common.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardDocumentItem {
    @ApiModelProperty(notes = "게시글 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "게시글 제목")
    private String title;

    @ApiModelProperty(notes = "작성자")
    private String writerName;

    @ApiModelProperty(notes = "조회수")
    private Integer viewCount;

    @ApiModelProperty(notes = "작성일")
    private LocalDateTime dateCreate;

    private BoardDocumentItem(Builder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.writerName = builder.writerName;
        this.viewCount = builder.viewCount;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<BoardDocumentItem> {
        private final Long id;
        private final String title;
        private final String writerName;
        private final Integer viewCount;
        private final LocalDateTime dateCreate;

        public Builder(BoardDocument boardDocument) {
            this.id = boardDocument.getId();
            this.title = boardDocument.getTitle();
            this.writerName = boardDocument.getWriterName();
            this.viewCount = boardDocument.getViewCount();
            this.dateCreate = boardDocument.getDateCreate();
        }

        @Override
        public BoardDocumentItem build() {
            return new BoardDocumentItem(this);
        }
    }
}
